<?php

namespace App\Actions;

use App\Models\Category;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Action;
use Illuminate\Support\Facades\Validator;

class StoreCategoryAction extends Action
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'parent_id' => 'nullable|exists:categories,id',
            'discount' => 'nullable|numeric|min:0',
        ];
    }

    public function handle(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $parent_id = $request->input('parent_id');
        $parentCategory = Category::find($parent_id);
        if ($parentCategory) {
            // Check maximum level of subcategories
            if ($parentCategory->tree_level() > 3) {
                return response()->json(['error' => 'Cannot add category below the fourth level.'], 422);
            }
            
            // Check if parent has mixed children
            if ($parentCategory->items()->exists()) {
                return response()->json(['error' => 'Parent category cannot have mixed children.'], 400);
            }
            if ($parentCategory->discount) {
                $request->merge(['inherited_discount' => $parentCategory->discount]);
            } else if ($parentCategory->inherited_discount) {
                $request->merge(['inherited_discount' => $parentCategory->inherited_discount]);
            }
        }

        $category = Category::create($request->all());
        $category->load('children');

        return response()->json($category, 201);
    }
}
