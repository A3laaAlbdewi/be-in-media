<?php

namespace App\Traits;

trait InheritedDiscountTrait
{
    public function updateInheritedDiscount($discount)
    {


        if ($this->children()->exists()) {
            $this->children()->update(['inherited_discount' => $discount]);
            $children = $this->children()->get();
            foreach ($children as $child) {
                if (!$child->discount) {
                    $child->updateInheritedDiscount($discount);
                }
            }
        } elseif ($this->items()->exists()) {
            $this->items()->update(['inherited_discount' => $discount]);
        }
    }

    public function deleteInheritedDiscount()
    {
        
        $parent = $this->parent()->first();
        if ($parent && $parent->inherited_discount) {
            $this->updateInheritedDiscount($parent->inherited_discount);
        }else{
            $this->updateInheritedDiscount($this->inherited_discount);
        }
    }
}