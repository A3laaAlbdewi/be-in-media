<?php

namespace App\Traits;

trait TreeLevelTrait
{
    public function tree_level()
    {
        $level = 0;
        $parent = $this->parent;
        
        while ($parent) {
            $level++;
            $parent = $parent->parent;
        }
        return $level;
    }
}

