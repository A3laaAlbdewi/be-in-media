<?php

namespace App\Models;

use App\Traits\TreeLevelTrait;
use App\Traits\InheritedDiscountTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory, InheritedDiscountTrait,TreeLevelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'inherited_discount',
        'discount'
    ];

    public function parent()
    {
    return $this->belongsTo(Category::class, 'parent_id');
    }
    
    public function children()
    {
    return $this->hasMany(Category::class, 'parent_id');
    }
    public function tree()
    {
    return $this->hasMany(Category::class, 'parent_id')->with(['tree','items']);
    }

    public function items()
    {
    return $this->hasMany(Item::class, 'category_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::updating(function ($category) {
            if ($category->isDirty('discount')) {
                $newDiscount = $category->discount;
                
                if (is_null($newDiscount)  || $newDiscount <= 0) {
                    $category->deleteInheritedDiscount();
                } else {
                    $category->updateInheritedDiscount($newDiscount);
                }
            }
        });
    }
}