<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'price',
        'inherited_discount',
        'discount',
        'category_id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = ['discounted_price'];
    public function category()
    {
    return $this->belongsTo(Category::class, 'category_id');
    }
    public function getDiscountedPriceAttribute()
    {
        if ($this->discount) {
            return $this->price - ($this->price * $this->discount /100);
        } elseif ($this->inherited_discount) {
            return $this->price - ($this->price * $this->inherited_discount /100);
        } else {
            return $this->price;
        }
    }
}
