<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Category;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    /**
     * Get all items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Item::query();

        if ($request->has('include') && $request->include === 'category') {
            $items->with('category');
        }

        $items = $items->get();

        return response()->json(['items' => $items], 200);
    }

    /**
     * Create a new item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|min:0',
            'discount' => 'numeric|min:0',
            'category_id' => 'required|exists:categories,id',
        ]);

        $category_id = $request->input('category_id');
        $parentCategory = Category::find($category_id);

        if ($parentCategory->children()->exists()) {
            return response()->json(['message' => 'Category or subcategory has mixed children.'], 422);
        }
        if ($parentCategory) {
            if ($parentCategory->discount) {
                $request->merge(['inherited_discount' => $parentCategory->discount]);
            } else if ($parentCategory->inherited_discount) {
                $request->merge(['inherited_discount' => $parentCategory->inherited_discount]);
            }else{
                $request->merge(['inherited_discount' => '']);
            }
        }else{
            $request->merge(['inherited_discount' => '']);
        }

        $item = Item::create($request->all());
        $item->load('category');

        return response()->json(['item' => $item], 201);
    }

    /**
     * Update an existing item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|min:0',
            'discount' => 'numeric|min:0',
            'category_id' => 'required|exists:categories,id',
        ]);

        $category_id = $request->input('category_id');
        $parentCategory = Category::find($category_id);
        if ($parentCategory->children()->exists()) {
            return response()->json(['message' => 'Category or subcategory has mixed children.'], 422);
        }
        if ($parentCategory) {
            if ($parentCategory->discount) {
                $request->merge(['inherited_discount' => $parentCategory->discount]);
            } else if ($parentCategory->inherited_discount) {
                $request->merge(['inherited_discount' => $parentCategory->inherited_discount]);
            }else{
                $request->merge(['inherited_discount' => '']);
            }
        }else{
            $request->merge(['inherited_discount' => '']);
        }

        $item = Item::find($id);
        $item->update($request->all());
        $item->load('category');

        return response()->json(['item' => $item], 200);
    }

    /**
     * Delete an item.
     *
     * @param    $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::find($id)->delete();

        return response()->json(['message' => 'Item deleted successfully'], 200);
    }

    /**
     * Get an item by ID.
     *
     * @param  Request $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Request $request,$id)
    {
        $item = Item::query();

        if ($request->has('include') && $request->include === 'category') {
            $item->with('category');
        }

        $item = $item->find($id);

        if (!$item) {
            return response()->json(['message' => 'Item not found'], 404);
        }

        return response()->json(['item' => $item], 200);
    }

}
