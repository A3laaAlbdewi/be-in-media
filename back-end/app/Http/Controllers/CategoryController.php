<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Actions\StoreCategoryAction;
use App\Actions\UpdateCategoryAction;

class CategoryController extends Controller
{

    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategory()
    {
        $categories = Category::where('parent_id',null)->get();

        return response()->json($categories);
    }

    /**
     * Get an item by ID.
     *
     * @param  Request $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoryById($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return response()->json(['message' => 'Category not found'], 404);
        }

        return response()->json($category);
    }
    
    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSupCategory()
    {
        $categories = Category::where('parent_id','!=',null)->get();

        return response()->json($categories);
    }

    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCategory()
    {
        $categories = Category::with('parent')->get();
            
        return response()->json($categories);
    }

    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllLeftCategory()
    {
        $categories = Category::doesntHave('children')->get();
            
        return response()->json($categories);
    }

    /**
     * Display the children categories or items of the specified category.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function children($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        $children = $category->children();

        if ($children->exists()) {
            $children = $children->get();
            return response()->json(['children' => $children]);
        } else {
            $items = $category->items()->get();
            return response()->json([ 'items' => $items]);
        }
    }

    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTreeCategory()
    {
        $categories = Category::where('parent_id',null)->with('tree')->get();

        return response()->json($categories);
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return StoreCategoryAction::run($request);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return UpdateCategoryAction::run($request, $id);
    }
    
    
    
    /**
     * Remove the specified category from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Category::find($id)->delete();
 
        return response()->json(['message' => 'Category deleted successfully'], 200);
    }
}
