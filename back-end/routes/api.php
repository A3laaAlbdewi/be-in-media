<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|
*/

Route::controller(AuthController::class)->group(function() {
    /*
    |--------------------------------------------------------------------------
    | APIs for Authentication (login,register)
    |--------------------------------------------------------------------------
    */
    Route::post('/register', 'register');
    Route::post('/login', 'login');
});

Route::middleware('auth:sanctum')->group( function () {

    Route::post('/logout', [AuthController::class, 'logout']);

    Route::controller(ItemController::class)->prefix('items')->group(function() {
        /*
        |--------------------------------------------------------------------------
        | APIs for items 
        |--------------------------------------------------------------------------
        */
        Route::post('/', 'store');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
    });

    Route::controller(CategoryController::class)->prefix('categories')->group(function() {
        /*
        |--------------------------------------------------------------------------
        | APIs for items 
        |--------------------------------------------------------------------------
        */
        

        Route::post('/', 'store');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
    });
    
});

Route::controller(ItemController::class)->prefix('items')->group(function() {
    /*
    |--------------------------------------------------------------------------
    | APIs for items 
    |--------------------------------------------------------------------------
    */

    Route::get('/',  'index');
    Route::get('/{id}',  'getById');
});

Route::controller(CategoryController::class)->prefix('categories')->group(function() {
    /*
    |--------------------------------------------------------------------------
    | APIs for categories 
    |--------------------------------------------------------------------------
    */

    Route::get('/getCategory', 'getCategory');
    Route::get('/getCategory/{id}', 'getCategoryById');
    Route::get('/getSupCategory', 'getSupCategory');
    Route::get('/getAllCategory', 'getAllCategory');
    Route::get('/getAllLeftCategory', 'getAllLeftCategory');
    Route::get('/children/{id}', 'children');
    Route::get('/getTreeCategory', 'getTreeCategory');
    
});
