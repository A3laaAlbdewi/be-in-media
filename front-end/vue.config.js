const { defineConfig } = require('@vue/cli-service');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = defineConfig({
  configureWebpack: {
    plugins: [
      new BundleAnalyzerPlugin(),
    ],
  },
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:8001', // Replace with your backend server URL
        changeOrigin: true,
        // You can add additional configurations if needed
      }
    }
  }
})

