import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginPage from '../components/Auth/LoginPage'
import RegisterPage from '../components/Auth/RegisterPage'
import Dashboard from '../views/DashboardPage.vue'
import items from '../components/Dashboard/items.vue'
import category from '../components/Dashboard/category.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterPage
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    children: [
      {
        path: '/items',
        name: 'items',
        component: items
      },
      {
        path: '/category',
        name: 'category',
        component: category
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
