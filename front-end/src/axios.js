import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: window.location.origin + `/api`, // Set your base URL
    headers: {
      'Content-Type': 'application/json',
      common:{
        'Authorization' : `Bearer ${localStorage.getItem('access_token')}`
      } 
    },
  });

  export default axiosInstance