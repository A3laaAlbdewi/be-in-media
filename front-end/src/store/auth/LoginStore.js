const loginModule = {
    namespaced: true,
    state: {
      isLogin: false,
      username: '',
      password: ''
    },
    mutations: {
      setIsLogin(state, value){
        state.isLogin = value
      },
      setUsername(state, username) {
        state.username = username
      },
      setPassword(state, password) {
        state.password = password
      }
    },
    getters: {
      getIsLogin(state){
        return state.isLogin
      },
      getUsername(state) {
        return state.username
      },
      getPassword(state) {
        return state.password
      }
    }
  }

  export default loginModule